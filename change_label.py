# -*- coding: utf-8 -*-
import random

def is_Chinese(word):
    """中文判断"""
    for ch in word:
        if '\u4e00' <= ch <= '\u9fff':
            return True
    return False

def change_label_onehot():
    cls_20 = ["sports_news", "art_and_entertainment_news", "social_news", "technology_news", "business_news",
              "political_news", "else_news", "daily_necessities", "fashion", "electronic_product", "leisure_products",
              "car", "else_shopping", "healthcare_services", "jobs", "education", "activity", "menu", "trip",
              "else_life"]
    cls_dic = {}
    for i in range(len(cls_20)):
        cls_dic[cls_20[i]] = i
    data = open('./data/dev1.tsv', 'r').readlines()
    f_out = open('./data/dev.tsv', 'w')
    for line in data:
        labels = ['0']*len(cls_20)
        line = line.strip().split('\t')
        if len(line) != 2:
            print(line)
            continue
        if line[1] == 'label':
            f_out.write(line[0] + '\t' + line[1] + '\n')
        try:
            index = cls_dic[line[1]]
        except:
            print(line)
            continue
        labels[index] = '1'
        label = ' '.join(labels)
        f_out.write(line[0] + '\t' + label + '\n')
    return


def change_label_index():
    cls_20 = ["sports_news", "art_and_entertainment_news", "social_news", "technology_news", "business_news",
              "political_news", "else_news", "daily_necessities", "fashion", "electronic_product", "leisure_products",
              "car", "else_shopping", "healthcare_services", "jobs", "education", "activity", "menu", "trip",
              "else_life"]
    cls_dic = {}
    for i in range(len(cls_20)):
        cls_dic[cls_20[i]] = i
    data = open('./data/train1.tsv', 'r').readlines() + open('./data/train_sec.tsv', 'r').readlines()
    random.shuffle(data)
    f_out = open('./data/train.tsv', 'w')
    for line in data:
        line = line.strip().split('\t')
        if len(line) != 2:
            print(line)
            continue
        if is_Chinese(line[0]):
            print(line)
            continue

        if line[1] == 'label':
            f_out.write(line[0] + '\t' + line[1] + '\n')
        try:
            index = cls_dic[line[1]]
        except:
            print(line)
            continue
        f_out.write(line[0] + '\t' + str(index) + '\n')
    return


if __name__ == "__main__":

    #change_label_onehot()
    change_label_index()
